require "spec_helper"
require "dashboard"

describe Dashboard do
  describe "#posts" do
    it "returns posts created today" do
      posts = double("posts_published_today")
      expect(Post).to receive(:today).and_return(posts)
      dashboard = Dashboard.new(posts: Post.all)

      expect(dashboard.posts).to eq posts
    end
  end

  around do |example|
    Timecop.freeze { example.run }
  end
end
